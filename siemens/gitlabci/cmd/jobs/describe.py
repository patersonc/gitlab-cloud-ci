# Copyright (c) Siemens AG, 2019
#
# Authors:
#  Michael Adler <michael.adler@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#

import os

import click

from siemens.gitlabci.cli import jobs as cli


@cli.command()
@click.argument("job_name")
@click.option(
    "--namespace", default="gitlab", help="Use the given namespace", show_default=True
)
def describe(job_name, namespace):
    """Describe a currently running CI job"""
    os.execlp(
        "kubectl",
        "kubectl",
        "describe",
        "pods/{}".format(job_name),
        "--namespace={}".format(namespace),
    )
