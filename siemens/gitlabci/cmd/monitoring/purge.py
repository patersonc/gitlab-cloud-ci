# Copyright (c) Siemens AG, 2020
#
# Authors:
#  Quirin Gylstorff <quirin.gylstorff@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#

import os

import click

from siemens.gitlabci.cli import monitoring as cli

if not os.getenv("_GITLABCI_COMPLETE", None):
    from sh import helm


@cli.command()
@click.option(
    "--namespace",
    default="monitoring",
    help="Deploy in the given namespace",
    show_default=True,
)
def purge(namespace):
    """Remove Monitoring from the cluster"""
    helm("uninstall", "grafana", "--namespace", namespace)
    helm("uninstall", "prometheus", "--namespace", namespace)
