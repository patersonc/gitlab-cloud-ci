# Copyright (c) Siemens AG, 2019
#
# Authors:
#  Michael Adler <michael.adler@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#

import os

import click

from siemens.gitlabci.cli import runner as cli

if not os.getenv("_GITLABCI_COMPLETE", None):
    from sh import helm


@cli.command()
@click.option(
    "--namespace", default="gitlab", help="Use the given namespace", show_default=True
)
def list(namespace):
    """List all runners in the given namespace"""
    print(_list(namespace))


def _list(namespace):
    """List all runners in the given namespace"""

    return helm("list", "--namespace", namespace)
