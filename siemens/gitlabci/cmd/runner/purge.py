# Copyright (c) Siemens AG, 2019
#
# Authors:
#  Michael Adler <michael.adler@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#

import os

import click

from siemens.gitlabci.cli import runner as cli

if not os.getenv("_GITLABCI_COMPLETE", None):
    from sh import helm


@cli.command()
@click.argument("name")
@click.option(
    "--namespace",
    default="gitlab",
    help="Deploy in the given namespace",
    show_default=True,
)
def purge(name, namespace):
    """Remove a Gitlab runner instance from the cluster"""
    helm("uninstall", name, "--namespace", namespace)
