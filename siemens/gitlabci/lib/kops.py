# Copyright (c) Siemens AG, 2021
#
# Authors:
#  Silvano Cirujano Cuesta <silvano.cirujano-cuesta@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#

import io
import logging

from sh import kops as sh_kops
from sh import ErrorReturnCode

log = logging.getLogger(__name__)


def kops(*kops_args, **sh_kwargs):
    with io.StringIO() as error_output:
        try:
            return sh_kops(*kops_args, **sh_kwargs, _err=error_output)
        except ErrorReturnCode:
            args = [str(arg) for arg in kops_args]
            log.error("kOps has failed calling: " + " ".join(args))
            for line in error_output.getvalue().splitlines():
                log.error(" >>> " + line)
            raise
