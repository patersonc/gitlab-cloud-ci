# Copyright (c) Siemens AG, 2019
#
# Authors:
#  Michael Adler <michael.adler@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#

import logging
import os

import kubernetes
from kubernetes.client.rest import ApiException

import sh
from sh import kubectl, kops

log = logging.getLogger(__name__)


def configure_kubernetes_client():
    kops("export", "kubecfg", "--admin")
    try:
        log.debug("Loading local kube config")
        kubernetes.config.load_kube_config()
        log.info("Successfully loaded local kube config")
    except FileNotFoundError:
        log.info("No kube config file found (yet)")
    except kubernetes.config.config_exception.ConfigException:
        log.warning("kubeconfig is invalid or incomplete")

    cfg = kubernetes.client.Configuration()
    cfg.proxy = os.getenv("https_proxy")
    kubernetes.client.Configuration.set_default(cfg)


def k8s_is_deployed(name, ns):
    try:
        kubernetes.client.AppsV1Api().read_namespaced_deployment_status(name, ns)
        return True
    except ApiException as e:
        if e.status == 404:
            return False
        else:
            raise e


def k8s_is_deployment_ready(name, ns):
    try:
        response = kubernetes.client.AppsV1Api().read_namespaced_deployment_status(
            name, ns
        )
        log.debug("deployment status: %s", response.status)
        if response.status.conditions:
            for condition in response.status.conditions:
                if condition.type == "Available":
                    status = condition.status
                    log.debug("deployment %s available status: %s", name, status)
                    return status == "True"
        else:
            return False
    except ApiException as e:
        if e.status == 404:
            return False
        else:
            raise e
    return False


def tolerate_deployment_on_master(name, ns):
    log.info(
        "Allowing deployment %s in namespace %s to be scheduled on the master node",
        name,
        ns,
    )
    patch = {
        "spec": {
            "template": {
                "spec": {
                    "tolerations": [
                        {
                            "key": "node-role.kubernetes.io/master",
                            "effect": "NoSchedule",
                        }
                    ]
                }
            }
        }
    }
    k8s_client_apps = kubernetes.client.AppsV1Api()
    k8s_client_apps.patch_namespaced_deployment(name, ns, patch)


def pin_deployment_on_master(name, ns):
    log.info("Pinning deployment %s in namespace %s on master node", name, ns)
    patch = {
        "spec": {
            "template": {
                "spec": {
                    "nodeSelector": {"dedicated": "master"},
                    "tolerations": [
                        {
                            "key": "node-role.kubernetes.io/master",
                            "effect": "NoSchedule",
                        }
                    ],
                }
            }
        }
    }
    k8s_client_apps = kubernetes.client.AppsV1Api()
    k8s_client_apps.patch_namespaced_deployment(name, ns, patch)


def add_dedicated_role_to_master():
    """add the the label dedicated:master to the k8s cluster"""
    k8s_client_core = kubernetes.client.CoreV1Api()
    for row in k8s_client_core.list_node().items:
        labels = row.metadata.labels
        if labels["kubernetes.io/role"] == "master" and "dedicated" not in labels:
            master_name = row.metadata.name
            log.info("Adding label dedicated=master to master node %s", master_name)
            k8s_client_core.patch_node(
                master_name, {"metadata": {"labels": {"dedicated": "master"}}}
            )


def is_cluster_up(node_count):
    """
    check if the cluster is up by getting the status of the master.
    The status is True if the master is ready.
    """
    try:
        # get the status of the last message
        status = kubectl(
            "get",
            "nodes",
            "--selector",
            "node-role.kubernetes.io/master",
            "-o",
            "custom-columns=:.status.conditions[-1].status",
        )
        node_status = status.stdout.decode("utf-8").strip().split()
        # check if all nodes are running
        if len(node_status) == 0:
            return False
        for state in node_status:
            if state != "True":
                return False
        return True
    except sh.ErrorReturnCode:
        return False
