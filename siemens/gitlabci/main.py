# Copyright (c) Siemens AG, 2019 - 2020
#
# Authors:
#  Michael Adler <michael.adler@siemens.com>
#
# SPDX-License-Identifier: Apache-2.0
#

import logging

import coloredlogs
import click_completion

from siemens.gitlabci.cli import cli
from siemens.gitlabci.cmd import *  # noqa: F401, F403
from siemens.gitlabci.cmd.autoscaler import *  # noqa: F401, F403
from siemens.gitlabci.cmd.dashboard import *  # noqa: F401, F403
from siemens.gitlabci.cmd.hardening import *  # noqa: F401, F403
from siemens.gitlabci.cmd.runner import *  # noqa: F401, F403
from siemens.gitlabci.cmd.create import *  # noqa: F401, F403
from siemens.gitlabci.cmd.destroy import *  # noqa: F401, F403
from siemens.gitlabci.cmd.jobs import *  # noqa: F401, F403
from siemens.gitlabci.cmd.monitoring import *  # noqa: F401, F403
from siemens.gitlabci.config import Config


def main():
    log = logging.getLogger()
    coloredlogs.install(
        logger=log,
        level="INFO",
        fmt="%(asctime)s,%(msecs)d %(levelname)-8s [%(filename)s:%(lineno)d] "
        "%(message)s",
        datefmt="[%Y-%m-%d] %H:%M:%S",
    )
    # monkey-patch click
    click_completion.init()

    cfg = Config("./config.json")
    cli(obj={"cfg": cfg})


if __name__ == "__main__":
    main()
